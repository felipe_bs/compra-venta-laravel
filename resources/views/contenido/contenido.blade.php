    @extends('principal')
    @section('contenido')

    @if(Auth::check())
            @if (Auth::user()->idrol == 1)
            <template v-if="menu==0">
                <dashboard-app></dashboard-app>
            </template>

            <template v-if="menu==1">
                <categoria-app></categoria-app>
            </template>

            <template v-if="menu==2">
                <articulo-app></articulo-app>
            </template>

            <template v-if="menu==3">
                <ingreso-app></ingreso-app>
            </template>

            <template v-if="menu==4">
                <proveedor-app></proveedor-app>
            </template>

            <template v-if="menu==5">
                <venta-app></venta-app>
            </template>

            <template v-if="menu==6">
                <cliente-app></cliente-app>
            </template>

            <template v-if="menu==7">
                <user-app></user-app>
            </template>

            <template v-if="menu==8">
                <rol-app></rol-app>
            </template>

            <template v-if="menu==9">
                <consultaingreso-app></consultaingreso-app>
            </template>

            <template v-if="menu==10">
                <consultaventa-app></consultaventa-app>
            </template>

            <template v-if="menu==11">
                <h1>Ayuda</h1>
            </template>

            <template v-if="menu==12">
                <h1>Acerca de</h1>
            </template>
            @elseif (Auth::user()->idrol == 2)
            <template v-if="menu==0">
                <dashboard-app></dashboard-app>
            </template>
            <template v-if="menu==5">
                <venta-app></venta-app>
            </template>

            <template v-if="menu==6">
                <cliente-app></cliente-app>
            </template>
            <template v-if="menu==10">
                <consultaventa-app></consultaventa-app>
            </template>

            <template v-if="menu==11">
                <h1>Ayuda</h1>
            </template>

            <template v-if="menu==12">
                <h1>Acerca de</h1>
            </template>
            @elseif (Auth::user()->idrol == 3)
            <template v-if="menu==0">
                <dashboard-app></dashboard-app>
            </template>
            <template v-if="menu==1">
                <categoria-app></categoria-app>
            </template>

            <template v-if="menu==2">
                <articulo-app></articulo-app>
            </template>

            <template v-if="menu==3">
                <ingreso-app></ingreso-app>
            </template>

            <template v-if="menu==4">
                <proveedor-app></proveedor-app>
            </template>
            <template v-if="menu==9">
                <consultaingreso-app></consultaingreso-app>
            </template>
            <template v-if="menu==11">
                <h1>Ayuda</h1>
            </template>

            <template v-if="menu==12">
                <h1>Acerca de</h1>
            </template>
            @else

            @endif

    @endif
       
        
    @endsection